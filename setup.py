from setuptools import find_packages, setup

setup(
    name='testing.mysql',
    version='0.1.1.4',
    description='MySQL testing framework',
    author='Joshua Haase',
    author_email="hahj87@gmail.com",
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    test_suite='tests'
)
