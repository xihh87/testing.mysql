import testing.mysql
import pytest  # noqa: F401
import sqlalchemy as sa


mysql = testing.mysql.DockerMySQL(
    {"username": "user", "password": "password", "port": "8888", "dbname": "db"}
)


def test_accept_database_info():
    mysql.url() == "mysql+pymysql://user:password@localhost:8888/db"


def test_copy_file_to_container():
    mysql.copy_file_to_container("data/schema.sql", "/var/lib/mysql-files")
    exit_code, _ = mysql.container.exec_run(["ls", "/var/lib/mysql-files/schema.sql"])
    assert exit_code == 0


def test_mysql_connection():
    conn = mysql.connect()
    conn.execute(sa.text("SELECT 1"))
    conn.close()


def test_run_sql_file_as_root():
    mysql.wait_until_ready()
    mysql.run_sql_file_as_root("data/schema.sql")
    try:
        conn = mysql.connect()
        [x for x in conn.execute(sa.text("select * from crops"))]
    finally:
        conn.close()


def test_load_data_on_table():
    mysql.wait_until_ready()
    mysql.load_data_on_table("data/producers.tsv", "producers")
    mysql.load_data_on_table("data/farms.tsv", "farms")
    mysql.load_data_on_table("data/fields.tsv", "fields")
    mysql.load_data_on_table("data/image_problems.tsv", "image_problems")
    mysql.load_data_on_table("data/images.tsv", "images")
    mysql.load_data_on_table("data/vistats.tsv", "vistats")
    conn = mysql.connect()
    try:
        result = [x for x in conn.execute(sa.text("select * from producers"))]
        assert len(result) == 1
    finally:
        conn.close()


def test_admin_connection():
    mysql.wait_until_ready()
    conn = mysql.admin()
    try:
        conn.execute(
            sa.text(
                "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"
            )
        )
    finally:
        conn.close()


def test_teardown():
    mysql.stop()


def test_defaults():
    new_db = testing.mysql.DockerMySQL()
    new_db.wait_until_ready()
    new_db.stop()
