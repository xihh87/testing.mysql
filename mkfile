help:QV:	## show this message
    cmd/show-help mkfile
start:V:	## create a dev environment
    python -m venv .venv
    .venv/bin/python -m pip install --upgrade pip
    .venv/bin/python -m pip install -r requirements.txt
test:VEQ:	## run tests in virtual environment
    .venv/bin/python -m black src tests
    .venv/bin/python -m flake8 --ignore E501 src tests
    env PYTHONPATH="$(realpath src)" .venv/bin/python -m pytest
pkg:V:    ## build the python package
    .venv/bin/python setup.py bdist_wheel
install:V:
    VERSION=0.1.1.2
    .venv/bin/python -m pip install dist/testing.mysql-${VERSION}-py3-none-any.whl
